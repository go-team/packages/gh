gh (2.46.0-3) unstable; urgency=medium

  [ Loren M. Lang ]
  * Apply patch to fix CVE-2024-54132 (Closes: #1089120)
  * Apply patch to fix CVE-2024-53858 (Closes: #1088808)
  * Don't let $GH_TOKEN from the build environment break the test
  * Fixed a build error introduced in a recent test change
  * Fixed an issue with flaky download tests
  * Enabled Salsa CI for packaging repository for improved quality
    assurance before uploads to Debian archives

  [ Otto Kekäläinen ]
  * Update copyright and uploaders metadata to match current maintainers

 -- Otto Kekäläinen <otto@debian.org>  Sun, 12 Jan 2025 21:43:14 -0800

gh (2.46.0-2) unstable; urgency=medium

  * Team upload.
  [ Otto Kekäläinen ]
  * Update test to be compatible with Glamour v0.8.0. Closes: #1091585.
  [ Santiago Vila ]
  * Update build-dependency on golang-github-charmbracelet-glamour-dev.
  [ Loren M. Lang ]
  * Apply patch to fix CVE-2024-52308. Closes: #1087883.

 -- Santiago Vila <sanvila@debian.org>  Mon, 30 Dec 2024 14:10:00 +0100

gh (2.46.0-1) unstable; urgency=medium

  * New upstream version 2.46.0
  * Bump versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Tue, 26 Mar 2024 17:51:24 -0600

gh (2.45.0-1) unstable; urgency=medium

  * New upstream version 2.45.0
  * Bump versioned dependency as per go.mod

 -- Anthony Fok <foka@debian.org>  Tue, 05 Mar 2024 09:48:51 -0700

gh (2.44.1-2) unstable; urgency=medium

  * Team upload
  * Skip failed TestHTTPClientSanitizeJSONControlCharactersC0 on go1.22.

 -- Shengjing Zhu <zhsj@debian.org>  Thu, 29 Feb 2024 19:35:33 +0800

gh (2.44.1-1) unstable; urgency=medium

  * New upstream version 2.44.1

 -- Anthony Fok <foka@debian.org>  Mon, 19 Feb 2024 11:28:08 -0700

gh (2.43.1-1) unstable; urgency=medium

  * New upstream version 2.43.1
  * Bump versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Wed, 07 Feb 2024 05:13:31 -0700

gh (2.42.1-1) unstable; urgency=medium

  * New upstream version 2.42.1
  * Bump versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Tue, 23 Jan 2024 18:54:24 -0700

gh (2.40.1+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.40.1+dfsg1
  * Update versioned dependencies as per go.mod
  * Remove github.com/cli/crypto fork originally for "ssh: support RSA
    SHA-2 (RFC8332) signatures"; see https://github.com/cli/cli/pull/8204
  * Bump local copy of google.golang.org/grpc from v1.49.0 to v1.56.3
  * Remove local copy of google.golang.org/grpc v1.49.0 in favour of
    Debian golang-github-golang-protobuf-1-5-dev (>= 1.5.3) package
  * Remove previously backported upstream 0002-go-md2man-fix.patch

 -- Anthony Fok <foka@debian.org>  Wed, 27 Dec 2023 13:20:19 -0700

gh (2.35.0-1) unstable; urgency=medium

  * New upstream version 2.35.0
  * Bump versioned dependencies as per go.mod
  * Remove temporary local copy of github.com/mislav/httpretty.
    The proposed changes in the github.com/mislav/httpretty has been merged
    upstream; see https://github.com/cli/cli/pull/7654. This reverts
    "Replace github.com/henvic/httpretty with github.com/mislav/httpretty"
  * Bump local copy of google.golang.org/grpc from v1.49.0 to v1.53.0
  * Refresh 0002-go-md2man-fix.patch

 -- Anthony Fok <foka@debian.org>  Mon, 04 Dec 2023 05:09:29 -0700

gh (2.30.0-2) unstable; urgency=medium

  * Team Upload.
  * Backport upstream patch to fix FTBFS with
    new go-md2man (Closes: #1054767)
  * Update versioned B-D on md2man

 -- Nilesh Patra <nilesh@debian.org>  Tue, 31 Oct 2023 02:58:01 +0530

gh (2.30.0-1) unstable; urgency=medium

  * New upstream version 2.30.0
  * Bump versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Sun, 15 Oct 2023 03:30:04 -0600

gh (2.27.0+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.27.0+dfsg1
  * Bump versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Mon, 03 Jul 2023 11:57:20 -0600

gh (2.24.3+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.24.3+dfsg1
  * Update versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Mon, 03 Jul 2023 11:20:00 -0600

gh (2.23.0+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.23.0+dfsg1
  * Bump build-dependencies according to go.mod:
    - golang-github-cli-oauth-dev (>= 1.0.1)
    - golang-github-joho-godotenv-dev (>= 1.5.1)
  * Replace github.com/henvic/httpretty with github.com/mislav/httpretty
    temporarily, as per go.mod, to "Avoid mangling response bodies of unknown
    length"; see https://github.com/henvic/httpretty/pull/17

 -- Anthony Fok <foka@debian.org>  Mon, 27 Feb 2023 12:41:45 -0700

gh (2.22.1+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.22.1+dfsg1

 -- Anthony Fok <foka@debian.org>  Wed, 01 Feb 2023 11:47:51 -0700

gh (2.22.0+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.22.0+dfsg1
  * Update versioned dependencies as per go.mod
  * Bump Standards-Version to 4.6.2 (no change)
  * Set GITHUB_ACTIONS to true to skip flaky tests;
    see https://github.com/cli/cli/discussions/6858

 -- Anthony Fok <foka@debian.org>  Thu, 26 Jan 2023 04:21:58 -0700

gh (2.18.1+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.18.1+dfsg1
  * Update versioned dependencies as per go.mod (as much as possible)
  * Add temporary local copy of google.golang.org/grpc v1.49.0
    and dependent github.com/golang/protobuf v1.5.2

 -- Anthony Fok <foka@debian.org>  Sat, 22 Oct 2022 06:57:00 -0600

gh (2.17.0+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.17.0+dfsg1
  * Build-depend explicitly on golang-any (>= 2:1.18~) as per go.mod

 -- Anthony Fok <foka@debian.org>  Tue, 04 Oct 2022 17:33:02 -0600

gh (2.16.1+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.16.1+dfsg1
  * Update versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Wed, 28 Sep 2022 07:49:05 -0600

gh (2.15.0+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.15.0+dfsg1
  * Bump dependency: golang-github-shurcool-githubv4-dev
    (>= 0.0~git20220115.a14260e)
  * Remove 0002-Bump-github.com-spf13-cobra-from-v1.4.0-to-v1.5.0.patch
    which has been applied upstream
  * debian/rules: Fix "gh version" changelogURL
    by removing +dfsg1 from build.Version in LDFLAGS

 -- Anthony Fok <foka@debian.org>  Tue, 06 Sep 2022 19:37:43 -0600

gh (2.14.7+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.14.7+dfsg1
  * Update versioned dependencies as per go.mod
  * Refresh 0001-set-defaultEditor-as-per-debian-policy.patch
  * Apply 0002-Bump-github.com-spf13-cobra-from-v1.4.0-to-v1.5.0.patch:
    Cobra v1.5.0 contains a fix for the zsh autocompletion #compdef line
    to only complete <command>, not _<command> the completion function itself,
    See https://github.com/cli/cli/pull/6196.  TestNewCmdCompletion for zsh,
    which checks that #compdef line, is updated accordingly.
    Fixes "FTBFS: FAIL: TestNewCmdCompletion/zsh_completion"
    with golang-github-spf13-cobra-dev (>= 1.5.0).
    Thanks to Shengjing Zhu for reporting the issue. (Closes: #1019137)

 -- Anthony Fok <foka@debian.org>  Mon, 05 Sep 2022 03:25:09 -0600

gh (2.14.4+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.14.4+dfsg1'
  * Update versioned dependency on golang-github-cli-go-gh-dev as per go.mod

 -- Anthony Fok <foka@debian.org>  Thu, 11 Aug 2022 19:05:26 -0600

gh (2.14.2+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.14.2+dfsg1
  * Update versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Mon, 18 Jul 2022 20:05:31 -0600

gh (2.12.1+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.12.1+dfsg1
  * Update versioned dependencies as per go.mod
  * Bump Standards-Version to 4.6.1 (no change)

 -- Anthony Fok <foka@debian.org>  Fri, 17 Jun 2022 06:58:09 -0600

gh (2.11.3+dfsg1-1) unstable; urgency=medium

  * New upstream version 2.11.3+dfsg1
  * Bump versioned dependencies as per go.mod

 -- Anthony Fok <foka@debian.org>  Sun, 05 Jun 2022 07:57:15 -0600

gh (2.4.0+dfsg1-4) unstable; urgency=medium

  * Revert "Add diversion for /usr/bin/gh to allow concurrent install
    with gitsome", and restore the versioned Conflicts with gitsome.
    Thanks to Antoine Beaupré for talking some sense into me especially
    this case is not like moreutils and parallel. (Closes: #1011545)

 -- Anthony Fok <foka@debian.org>  Wed, 01 Jun 2022 16:55:59 -0600

gh (2.4.0+dfsg1-3) unstable; urgency=medium

  * Limit "Conflicts: gitsome" to older (<< 0.8.0+ds-7.1) versions.
    Thanks to Antoine Beaupre for the suggestion, and for resolving the
    file conflict with gitsome (#1005858) so amicably! (Closes: #1011545)
  * Change default editor from nano to /usr/bin/editor as per Debian Policy
    §11.4. Thanks to Jakub Wilk for the bug report. (Closes: #1008761)
  * Rename "Build-Using" field to "Static-Build-Using" in debian/control
  * Add diversion for /usr/bin/gh to allow concurrent install with gitsome
    and remove Conflicts with gitsome. This is inspired by the conflict
    resolution between the moreutils and parallel packages where both
    contain /usr/bin/parallel.  See discussions in #749355.

 -- Anthony Fok <foka@debian.org>  Tue, 31 May 2022 01:58:33 -0600

gh (2.4.0+dfsg1-2) unstable; urgency=medium

  * Add "Conflicts: gitsome" which also provides /usr/bin/gh.
    Thanks to Axel Beckert for the bug report. (Closes: #1005858)
  * Source-only upload for migration to testing

 -- Anthony Fok <foka@debian.org>  Wed, 23 Mar 2022 04:10:32 -0600

gh (2.4.0+dfsg1-1) unstable; urgency=medium

  * Initial release (Closes: #951374)
  * Remove script/signtool.exe from source tarball.
    SignTool from Windows SDK is not open-source software, see
    https://docs.microsoft.com/en-us/windows/win32/seccrypto/signtool
    Thanks to Thorsten Alteholz for the advice regarding signtool.exe
  * Adjust debian/watch to add +dfsg1 suffix,
    to skip upstream's v9.9.9-test and pre-releases,
    and to fetch all past tags from api.github.com

 -- Anthony Fok <foka@debian.org>  Tue, 15 Feb 2022 00:56:31 -0700

gh (2.4.0-1) UNRELEASED; urgency=medium

  * Initial pre-release

 -- Anthony Fok <foka@debian.org>  Mon, 03 Jan 2022 13:07:01 -0700
